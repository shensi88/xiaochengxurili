// userCenter.js
Page({
  data: {
    imagePath:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1494927304237&di=3fb6569739e27f3015d7508d27bd65f2&imgtype=0&src=http%3A%2F%2Fpic.nipic.com%2F2008-03-11%2F200831117718530_2.jpg',
    title:['我的订单','我的水票','地址管理']
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },
  pushVC: function (event){//页面跳转
    var idxId = event.currentTarget.dataset.idx
    if (idxId == 0){
      wx.navigateTo({
        url: '/pages/myOrder/myOrder',
      })
    }
    if (idxId == 1) {
      wx.navigateTo({
        url: '/pages/changeAddress/changeAddress',
      })
    }
    if (idxId==2){
      wx.navigateTo({
        url: '/pages/addressList/addressList',
      })
    }
   
  }
})