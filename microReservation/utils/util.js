function formatTime(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()


  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}
//获取时间年/月/日
function formatTimes(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/');
}
//获取时间年-月-日
function formatDateNow(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()
  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()
  return [year, month, day].map(formatNumber).join('-');
}
//获取当前时间前一个月的时间
function formatBeforeMonth(date) {
  //获取系统前一个月的时间
  date.setMonth(date.getMonth() - 1);
  var y = date.getFullYear();
  var m = date.getMonth() + 1;
  var d = date.getDate();
  return [y, m, d].map(formatNumber).join('-');
}
function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}
//将时间戳转换为时间格式
function toDate(number) {
  var n = number * 1000;
  var date = new Date(n);
  var Y = date.getFullYear() + '/';
  var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '/';
  var D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
  return (Y + M + D + '(' + formatWeekday(number) + ')')
}
//将时间戳格式化为日期
function formatDateNum(timestamp) {
  var date = new Date(timestamp * 1000);
  return date.getMonth() + 1 + "月" + date.getDate() + "日 " + formatWeekday(timestamp);
}

//将时间戳格式化为时间
function formatTimeNum(timestamp) {
  var date = new Date(timestamp * 1000);
  return date.getHours() + ":" + date.getMinutes();

}
//中文形式的每周日期
function formatWeekday(timestamp) {

  var date = new Date(timestamp * 1000);
  var weekday = ["周日", "周一", "周二", "周三", "周四", "周五", "周六"];
  var index = date.getDay();
  return weekday[index];
}
//获取用户信息
function getUserInfo() {
  wx.getUserInfo({
    success: function (res) {
      var userInfo = res.userInfo
      var encryptedData = res.encryptedData
      var iv = res.iv
      return { 'userInfo': userInfo, 'encryptedData': encryptedData, 'iv': iv, 'success': true }
    },
    fail: function (res) {
      console.log('获取用户资料失败')
      return { 'success': false }
    }
  });

  return [year, month, day].map(formatNumber).join('-');
}

//缓存到本地
function setStorageKey(key, data) {
  wx.setStorageSync({
    key: key,
    data: data,
    success: function (res) {
      return true
    },
    fail: function (e) {
      return false
    }
  })
}
module.exports = {
  formatTime: formatTime,
  formatTimes: formatTimes,
  toDate: toDate,
  formatDateNow: formatDateNow,
  formatDateNum: formatDateNum,
  formatTimeNum: formatTimeNum,
  formatBeforeMonth: formatBeforeMonth,
  getUserInfo: getUserInfo,
  setStorageKey: setStorageKey
}

