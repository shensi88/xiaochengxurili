Page({
  data:{
    imageUrl:[
      "http://pic29.photophoto.cn/20131007/0036036873102160_b.jpg",
      "http://img0.imgtn.bdimg.com/it/u=844837588,1717528368&fm=214&gp=0.jpg",
      "http://d.5857.com/wmzw_170309/001.jpg"
    ],
    indicatorDots: true,
    autoplay: true,
    interval: 2000,
    duration: 1000,
    circular:true,

    listData: [
      {
      index: 0,
      title: '美容美甲预约场景',
      distance: '400',
      locationAddrss:'贵阳市云岩区贵开路居易阁10楼1号',
imagePath:'http://d.5857.com/wmzw_170309/001.jpg'
    },{
      index: 1,
      title: '编程培训预约场景',
      distance: '100',
      locationAddrss:'贵阳市云岩区贵开路居易阁10楼1号',
imagePath:'http://d.5857.com/wmzw_170309/001.jpg'
    }
    ]

  },
  tapPush:function(event){
      wx.navigateTo({  
      url: '../shopDetails/shopDetails',  
      success: function(res){  
        // success  
      },  
      fail: function() {  
        // fail  
      },  
      complete: function() {  
        // complete  
      }  
    }) 
  },
  onLoad:function(options){
    // 生命周期函数--监听页面加载
    
  },
  onReady:function(){
    // 生命周期函数--监听页面初次渲染完成
    
  },
  onShow:function(){
    // 生命周期函数--监听页面显示
    
  },
  onHide:function(){
    // 生命周期函数--监听页面隐藏
    
  },
  onUnload:function(){
    // 生命周期函数--监听页面卸载
    
  },
  onPullDownRefresh: function() {
    // 页面相关事件处理函数--监听用户下拉动作
    
  },
  onReachBottom: function() {
    // 页面上拉触底事件的处理函数
    
  },
  onShareAppMessage: function() {
    // 用户点击右上角分享
    return {
      title: 'title', // 分享标题
      desc: 'desc', // 分享描述
      path: 'path' // 分享路径
    }
  }
})