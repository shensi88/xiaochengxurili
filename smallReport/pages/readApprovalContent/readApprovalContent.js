var url='https://hd.gzfzkj.com/xbgapi/Report/GetInfo';
var treateurl ='https://hd.gzfzkj.com/xbgapi/Report/Treate';
var urlHeard='https://hd.gzfzkj.com';
Page({
  data:{
    rid:'',
    userHeardImage:'',
    title:'',
    name:'',
    uploadTime:'',
    uploadAddress:'',
    content:'',
    enclosure:[],
    approvalText:'',
    treatetype:0,
    formId:null,
    treatetypeShow:'0',
    treateremark:null,
    showType:true,
    treatetypename:'',
    isStart:false,
  },
  showMacImageView:function(event){//点击放大图片
         var that = this
         var enclosure = that.data.enclosure
         console.log('点击放大图片')
         wx.previewImage({
            current: enclosure[0], // 当前显示图片的http链接
            urls: enclosure // 需要预览的图片http链接列表
})
},
  // var idxId = event.currentTarget.dataset.idx
  putInApproval:function(e){//输入意见
    var that = this
    var approvalText = that.data.approvalText
    that.setData({
      approvalText: e.detail.value
    })
  },
  addApproval: function (event) {
    console.log('点击的下表为携带数据为：' + event.currentTarget.dataset.idx + 1)
    var that = this
    var treatetype = that.data.treatetype
    that.setData({
      treatetype: event.currentTarget.dataset.idx + 1
    })
    console.log('点击的下表为：' + that.data.treatetype)
    if (that.data.formId.length != 0 && that.data.treatetype != 0 && that.data.formId!=null) {
      if (!that.data.isStart){
        that.addDataRequest();
    }
    }
  },
  formSubmit: function (e) {
    console.log('开始进入方法' + e.detail.formId)
    var that = this
    var formId = that.data.formId
    that.setData({
      formId: e.detail.formId
    })
    if (that.data.formId.length != 0 && that.data.treatetype != 0 && that.data.formId != null) {
      if (!that.data.isStart){
      that.addDataRequest();
    }
    }
  },
  addDataRequest:function(){
    var that =this
    that.dataisStart = true;
    console.log('id' + that.data.formId+'  ' + 'type' + that.data.treatetype)
    // 获取openid
    wx.getStorage({
      key: 'openId',
      success: function (res) {
        var openId = res.data
        wx.showLoading({
          title: '正在提交',
        })
        console.log('开始进行请求方法' + that.data.treatetypeId)
        // 获取openid
        wx.request({
          url: treateurl,
          data: {
            treateopenid: openId,
            rid: that.data.rid,
            treateremark: that.data.approvalText,
            treatetype: that.data.treatetype,
            formid: that.data.formId,
          },
          method: 'POST',
          header: { 'Content-Type': 'application/x-www-form-urlencoded' },
          success: function (res) {
            console.log('返回的数据是' + res.data.ret)
            if (res.data.ret == 0) {
              wx.showToast({
                title: ' 提交成功',
                icon: 'success',
                duration: 1000
              })
              wx.hideLoading();
              wx.setStorage({
                key: "myApproListFresh",
                data: true,
                success: function (res) {
                  console.log('刷新')
                },
                fail: function () {
                  console.log('存到本地的刷新状态失败')
                  wx.showToast({
                    title: ' 存到本地的刷新状态失败',
                    icon: 'success',
                    duration: 1000
                  })
                }
              })

              wx.navigateBack({
                delta: 1
              })
            } else {
              wx.showToast({
                title: ' 提交失败',
                icon: 'fail',
                duration: 1000
              })
              wx.hideLoading()

            }
          }
        })
      }
    })
  },
  onLoad:function(options){
    // 生命周期函数--监听页面加载
    // console.log('获取到的数据是'+options.rid)
    var that = this
    wx.showLoading({
         title:'正在加载……',
         mask:true,
       })
          wx.request({
          url: url,
          data: {
           'rid': options.rid,
          }, 
          method: "GET",
          header: { 'Content-Type': 'application/x-www-form-urlencoded'},
          success: function (res) {
            console.log('返回的数据是' + res.data.data.treateremark)
            // console.log('获取的数据是' + res.data.data.treatetype)
            var userHeardImage = that.data.userHeardImage
            var title = that.data.title
            var name = that.data.name
            var uploadTime = that.data.uploadTime
            var uploadAddress = that.data.uploadAddress
            var content = that.data.content
            var enclosure = that.data.enclosure
            var treatetype = that.data.treatetype
            var treatetypeShow = that.data.treatetypeShow
            var listImage=[]
            var showType = that.data.showType
            var showOrhiden=true
            var rid = that.data.rid
            var treateremark = that.data.treateremark
            var treatetypename = that.data.treatetypename
            if(res.data.data.imageone!=null&&res.data.data.imageone.length!=0){
                  listImage.push(urlHeard+res.data.data.imageone);
            }
            if(res.data.data.imagetwo!=null&&res.data.data.imagetwo.length!=0){
                  listImage.push(urlHeard+res.data.data.imagetwo);
            }
            if (res.data.data.treatetype == treatetypeShow) {
              showOrhiden = true
            }else{
              showOrhiden = false
            }
            that.setData({
                userHeardImage:res.data.data.speakerheadimgurl,
                title:res.data.data.title,
                name:res.data.data.speakernickname, 
                uploadTime:res.data.data.createdate,
                uploadAddress:res.data.data.address,
                content:res.data.data.content,
                enclosure:listImage,
                treatetype:res.data.data.treatetype,
                showType: showOrhiden,
                rid: options.rid,
                treateremark: res.data.data.treateremark,
                treatetypename: res.data.data.treatetypename,
            })
            console.log(that.data.enclosure)
  
             wx.hideLoading()
            wx.showToast({
             title: ' 获取成功',
             icon: 'success',
             duration: 1500
            })
      },
      fail:function(res){
        wx.stopPullDownRefresh()
             wx.hideLoading()
             wx.showToast({
             title: res.data.msg,
             icon: 'fail',
             duration: 1500
            })
      }
    }) 
  },
  onReady:function(){
    // 生命周期函数--监听页面初次渲染完成
    
  },
  onShow:function(){
    // 生命周期函数--监听页面显示
    
  },
  onHide:function(){
    // 生命周期函数--监听页面隐藏
    
  },
  onUnload:function(){
    // 生命周期函数--监听页面卸载
    
  },
  onPullDownRefresh: function() {
    // 页面相关事件处理函数--监听用户下拉动作
    
  },
  onReachBottom: function() {
    // 页面上拉触底事件的处理函数
    
  },
  onShareAppMessage: function() {
    // 用户点击右上角分享
    return {
      title: 'title', // 分享标题
      desc: 'desc', // 分享描述
      path: 'path' // 分享路径
    }
  }
})