var url='https://hd.gzfzkj.com/xbgapi/Report/GetInfo';
var urlHeard='https://hd.gzfzkj.com';
Page({
  data:{
    userHeardImage:'',
    title:'',
    name:'',
    uploadTime:'',
    uploadAddress:'',
    content:'',
    enclosure:[],
    treateremark:'',
    treatetype:'',
    treatetypename:'',
    rid:'',
    listImagePath:'/image/delect.png'
  },
  showMacImageView:function(event){//点击放大图片
         var that = this
         var enclosure = that.data.enclosure
         console.log('点击放大图片')
         wx.previewImage({
            current: enclosure[0], // 当前显示图片的http链接
            urls: enclosure // 需要预览的图片http链接列表
})
  },
  onLoad:function(options){
    // 生命周期函数--监听页面加载
    console.log('获取到的数据是'+options.rid)
    var that = this
    wx.showLoading({
         title:'正在加载……',
         mask:true,
       })
          wx.request({
          url: url,
          data: {
           'rid': options.rid,
          }, 
          method: "GET",
          header: { 'Content-Type': 'application/x-www-form-urlencoded'},
          success: function (res) {
            console.log('获取的数据是' + res.data.data.treatetype)
            var userHeardImage = that.data.userHeardImage
            var title = that.data.title
            var name = that.data.name
            var uploadTime = that.data.uploadTime
            var uploadAddress = that.data.uploadAddress
            var content = that.data.content
            var enclosure = that.data.enclosure
            var listImage=[]
            var treateremark = that.data.treateremark
            var treatetype = that.data.treatetype
            var treatetypename = that.data.treatetypename
            var rid = that.data.rid
            if(res.data.data.imageone!=null&&res.data.data.imageone.length!=0){
                  listImage.push(urlHeard+res.data.data.imageone);
            }
            if(res.data.data.imagetwo!=null&&res.data.data.imagetwo.length!=0){
                  listImage.push(urlHeard+res.data.data.imagetwo);
            }
            that.setData({
                userHeardImage:res.data.data.speakerheadimgurl,
                title:res.data.data.title,
                name:res.data.data.speakernickname, 
                uploadTime:res.data.data.createdate,
                uploadAddress:res.data.data.address,
                content:res.data.data.content,
                enclosure:listImage,
                treatetypename: res.data.data.treatetypename,
                treateremark: res.data.data.treateremark,
                rid: options.rid,
            })
            console.log(that.data.enclosure)
            wx.stopPullDownRefresh()
             wx.hideLoading()
            wx.showToast({
             title: ' 获取成功',
             icon: 'success',
             duration: 1500
            })

      },
      fail:function(res){
        wx.stopPullDownRefresh()
             wx.hideLoading()
             wx.showToast({
             title: res.data.msg,
             icon: 'fail',
             duration: 1500
            })
      }
    }) 
  },
  onReady:function(){
    // 生命周期函数--监听页面初次渲染完成
    
  },
  onShow:function(){
    // 生命周期函数--监听页面显示
    
  },
  onHide:function(){
    // 生命周期函数--监听页面隐藏
    
  },
  onUnload:function(){
    // 生命周期函数--监听页面卸载
    
  },
  onPullDownRefresh: function() {
    // 页面相关事件处理函数--监听用户下拉动作
    
  },
  onReachBottom: function() {
    // 页面上拉触底事件的处理函数
    
  },
  onShareAppMessage: function() {
    // 用户点击右上角分享
    var that = this
    return {
      title: '报告审批请求', // 分享标题
      desc: that.data.title, // 分享描述
      path: '/pages/readApprovalContent/readApprovalContent?rid=' + that.data.rid// 分享路径
    }
  }
})