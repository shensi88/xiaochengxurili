var util = require('../../utils/util.js');
var url = "https://hd.gzfzkj.com/xbgapi/Report/GetTreateList";
var page = 0;
var pagesize = 20;
var app = getApp();
var GetList = function (that) {
  console.log('do somthing1' + page)
  if (page == 0) {
    that.setData({
      list: [],
    })
  }
  wx.showLoading({
    title: '正在加载……',
    mask: true,
  })
  // 获取openid
  wx.getStorage({
    key: 'openId',
    success: function (res) {
      var openId = res.data
      // 获取openid
      // 请求列表
      var begintime = that.data.startTime
      var endtime = that.data.endTime
      wx.request({
        url: url,
        data: {
          pageindex: page,
          pagesize: pagesize,
          treateopenid: openId,
          begintime: begintime,
          endtime: endtime
        },
        method: 'GET',
        header: { 'Content-Type': 'application/x-www-form-urlencoded' },
        success: function (res) {
          if (res.data.ret == 0) {
            // 将页面刷新状态取消
            wx.setStorage({
              key: "myApproListFresh",
              data: false,
              success: function (res) {
                console.log('刷新')
              },
              fail: function () {
                console.log('存到本地的刷新状态失败')
              }
            })
// 将页面刷新状态取消
            var list = that.data.list;
            for (var i = 0; i < res.data.list.length; i++) {
              var time = util.toDate(res.data.list[i].create_time);
              res.data.list[i].create_time = time;
              list.push(res.data.list[i]);
            }
            that.setData({
              list: list
            });
            page++;
          } else {
            wx.showToast({
              title: res.data.msg,
              icon: 'fail',
              duration: 1500
            })
          }
          wx.stopPullDownRefresh()
          wx.hideLoading()
        },
        fail: function (res) {
          wx.showToast({
            title: '获取数据失败',
            icon: 'fail',
            duration: 1500
          })
        }
      });
    },
    fail: function () {
      wx.showToast({
        title: '获取用户ID失败',
        icon: 'fail',
        duration: 1500
      })
    }
  })
}
Page({
  data: {
    topImagePath: '/image/guangGao.png',
    list: [],
    startTime: '',
    endTime: '',
    startPickEndTime: '',
    treatetype: null,
    noDataText: '暂无数据',
    authDeny: 0,//0不现实任何东西，表示第一次进来，1表示授权成功，2表示不同意授权
    showText: '小程序需要您的微信授权才能使用'
  },
  pushReportContent(event) {// pushReportContent 跳转详情的方法
    var idxId = event.currentTarget.dataset.idx
    var that = this
    var rid = that.data.list[idxId].rid
    console.log('点击了' + rid)
    wx.navigateTo({
      url: '/pages/readApprovalContent/readApprovalContent?rid=' + rid,
      success: function (res) {
        // success
      },
      fail: function (res) {
        // fail
      },
      complete: function (res) {
        // complete
      }
    })
  },
  selectStartTime: function (e) {
    var that = this
    var startTime = that.data.startTime
    this.setData({
      startTime: e.detail.value
    })
  },
  selectEndTime: function (e) {
    var that = this
    var endTime = that.data.startTime
    this.setData({
      endTime: e.detail.value
    })
    // 刷新数据
    page = 0;
    this.setData({
      list: [],
      // scrollTop : 0
    });
    GetList(that)
    // 刷新数据
  },
  setting: function () {//前往设置
    var that = this
    wx.openSetting({
      success(res) {
        var authDeny = that.data.authDeny
        console.log(res)
        if (res.errMsg == 'openSetting:ok') {
          that.setData({
            authDeny: 1
          })
          that.getUsetInfoNew();
        } else {
          that.setData({
            authDeny: 2
          })
        }
      }
    })

  },
  onLoad: function (options) {
    // 生命周期函数--监听页面加载
    var that = this
    var time = util.formatDateNow(new Date);//获取当前时间
    var startPickEndTime = util.formatDateNow(new Date);//获取当前时间,这是是开始时间选择器的结束时间
    var endTime = that.data.endTime
    that.setData({//获取当前日期
      endTime: time,
      startPickEndTime: startPickEndTime
    });
    var startTime = that.data.startTime
    var startBefore = util.formatBeforeMonth(new Date);//获取当前时间前一个月
    that.setData({//获取当前前一个月日期
      startTime: startBefore
    });
    wx.getStorage({
      key: 'openId',
      success: function (res) {
        var openId = res.data
        console.log('openId' + openId)
        // 刷新数据
        page = 0;
        that.setData({
          list: [],
          authDeny: 1
          // scrollTop : 0
        });
        GetList(that)
        // 刷新数据
      },
      fail: function (res) {
        that.getUsetInfoNew();
      }
    })

  },
  getUsetInfoNew: function () {
    var that = this
    wx.getUserInfo({
      success: function (res) {
        // 请求==opened
        var encryptedData = res.encryptedData
        var iv = res.iv
        console.log('iv:' + iv + ' encryptedData: ' + app.data.sessionIdUser)
        wx.showLoading({
          title: '正在获取用户信息',
          mask: true,
        })
        wx.request({
          url: 'https://hd.gzfzkj.com/xbgapi/Index/DecodeEncryptedData',
          method: 'POST',
          header: { 'Content-Type': 'application/x-www-form-urlencoded' },
          data: {
            dtype: 'USERINFO',
            encryptedData: encryptedData,
            iv: iv,
            sessionId: app.data.sessionIdUser
          },
          success: function (res) {
            var openId = res.data.decodedEntity.openId
            console.log('获取用户openId成功' + openId)
            wx.setStorage({
              key: 'openId',
              data: openId,
              success: function () {
                wx.hideLoading()

                // 刷新数据
                page = 0;
                that.setData({
                  list: [],
                  authDeny: 1
                  // scrollTop : 0
                });
                GetList(that)
                // 刷新数据
              },
            })

          },
          fail: function (obj) {

          },
        })
      },
      fail: function (res) {
        if (res.errMsg == "getUserInfo:cancel" || res.errMsg == "getUserInfo:fail auth deny") { //用户未授权
          wx.showModal({
            title: '提示',
            content: '使用小程序需要您进行授权',
            success: function (res) {
              if (res.confirm) {
                that.setting();
              } else if (res.cancel) {
                var authDeny = that.data.authDeny
                that.setData({
                  authDeny: 2
                })
              }
            }
          })
        }
      }
    })
    // 请求==opened
  },
  onReady: function () {
    // 生命周期函数--监听页面初次渲染完成

  },
  onShow: function () {
    // 生命周期函数--监听页面显示
    console.log('-------')
    //刷新页面
    var that = this
    wx.getStorage({
      key: 'openId',
      success: function (res) {
        var openId = res.data
        if (res.data) {
          var authDeny = that.data.authDeny
          if (authDeny != 1) {
            // 刷新数据
            page = 0;
            that.setData({
              list: [],
              authDeny: 1

            });
            GetList(that)
          }
        }
      }
    })

    wx.getStorage({
      key: 'myApproListFresh',
      success: function (res) {
        if (res.data) {
          page = 0;
          that.setData({
            list: [],
            // scrollTop : 0
          });
          GetList(that)
        }
      }
    })
  },
  onHide: function () {
    // 生命周期函数--监听页面隐藏

  },
  onUnload: function () {
    // 生命周期函数--监听页面卸载

  },
  onPullDownRefresh: function () {
    // 页面相关事件处理函数--监听用户下拉动作
    var that = this
    wx.getStorage({
      key: 'openId',
      success: function (res) {
        page = 0;
        that.setData({
          list: [],
          // scrollTop : 0
        });
        GetList(that)
      },
      fail: function () {
        that.getUsetInfoNew();
      }
    })
  },
  onReachBottom: function () {
    // 页面上拉触底事件的处理函数
    if (page != 0) {
      GetList(this)
    }//因为下拉刷新回弹的时候会触发上啦刷新，导致重复取值page为0 的数据，所以加一个判断，当page不为0的时候才可以上啦刷新
  },
  onShareAppMessage: function () {
    // 用户点击右上角分享
    return {
      title: '小报告', // 分享标题
      desc: '小报告', // 分享描述
      path: '/pages/reportList/reportList' // 分享路径
    }
  }
})