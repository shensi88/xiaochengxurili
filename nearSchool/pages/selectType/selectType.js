// selectType.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  checkboxChange:function(e){//选择类型

  },
  finish:function(){//点击完成按钮
   var that = this
   that.fresh();
  },
  fresh: function () {//设置上一个页面进行刷新,并返回上一页面
    var that = this
    var pagelist = getCurrentPages();
    console.log(pagelist)
    if (pagelist.length > 1) {
      //获取上一个页面实例对象
      var prevPage = pagelist[pagelist.length - 2];
      prevPage.setData({
        fresh: true
      })
    }
    wx.navigateBack({
      url: 'B',
    })
  }

})