// settledIn.js
var app = new getApp();
var util = require('../../utils/util.js');
function countdown(that) {
  var second = that.data.second
  if (second == 0) {
    that.setData({
      second: 60,
      buttonText: '发送验证码'
    });
    return;
  }
  var time = setTimeout(function () {
    that.setData({
      second: second - 1,
      buttonText: second - 1 + 's'
    });
    countdown(that);
  }
    , 1000)
}
Page({

  /**
   * 页面的初始数据
   */
  data: {
    buttonText: '发送验证码',
    second: 60,
    positiveImage: [],
    negativeImage: [],
    coverImage: [],
    addressGPS: '',//定位的经纬度
    longitude: '',//经度
    latitude: '',//纬度
    mainBusiness: '',//主营业务

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  positive: function (e) {//选择正面图片
    var that = this
    wx.chooseImage({
      count: 1,
      success: function (res) {
        var tempFilePaths = res.tempFilePaths
        that.setData({
          positiveImage: tempFilePaths
        })
      },
    })
  },
  negative: function (e) {//选择反面图片
    var that = this
    wx.chooseImage({
      count: 1,
      success: function (res) {
        var tempFilePaths = res.tempFilePaths
        that.setData({
          negativeImage: tempFilePaths
        })
      },
    })
  },
  sendCode: function () {//发送验证码
    var that = this
    countdown(that);
  },
  addData: function () {//提交数据审核

  },
  selectCoverImage: function () {//选择机构封面图片
    var that = this
    wx.chooseImage({
      count: 1,
      success: function (res) {
        var tempFilePaths = res.tempFilePaths
        that.setData({
          coverImage: tempFilePaths
        })
      },
    })
  },
  pushProtocol: function () {//跳转协议阅读页面

  },
  pushSelectGPS: function () {//跳转地址采集页面
    var that = this

    wx.chooseLocation({
      success: function (res) {
        console.log(res.latitude + res.longitude)
        var latitude = res.latitude.toFixed(6).toString();
        var longitude = res.longitude.toFixed(6).toString();
        that.setData({
          latitude: latitude,
          longitude: longitude,
          addressGPS: '纬度:' + latitude + ' ' + '经度:' + longitude
        })

      },
    })
  },
  pushMainBusiness: function () {//跳转主营业务选择页面
    wx.navigateTo({
      url: '/pages/selectType/selectType',
    })
  },
})