// home.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    infoList: [{ image: '/image/location.png', title: '地址', content: '贵州省贵阳市云岩区贵开路居易阁10楼1号' },
    { image: '/image/phone.png', title: '电话', content: '0851-86767827' },
    { image: '/image/time.png', title: '营业时间', content: '08:00-21:00' }],
    typeLIst: [
      { title: '艺术培训', image: '/image/art.png'},
      { title: '初小辅导', image: '/image/career.png' },
      { title: '婴幼教育', image: '/image/children.png' },
      { title: '语言培训', image: '/image/language.png' },
      { title: '体育培训', image: '/image/physical.png' },
      { title: '职业培训', image: '/image/counseling.png' },
      { title: '学历教学', image: '/image/education.png' },
      { title: '餐饮培训', image: '/image/food.png' },
      { title: '驾校培训', image: '/image/driving.png' },
      { title: '留学服务', image: '/image/abroad.png' },
    ],
    coursesList:[
      { title: 'iOS软件开发', image: '/image/iOS.png', price: '29880', originalPrice:'30000'},
      { title: '厨师培训', image: '/image/chef.png', price: '2988', originalPrice: '3000' },
      { title: '英语培训', image: '/image/english.png', price: '1000', originalPrice: '2000' },
      { title: 'iOS软件开发', image: '/image/web.png', price: '29880', originalPrice: '30000' },
      ],
    hotSchool: [
      { title: '方阵培训', image: '/image/fz.png', km: '298', address: '贵阳市贵开路居易阁10楼1号' },
      { title: '星火培训', image: '/image/2.png', km: '298', address: '贵阳市南明区龙洞堡' },
      { title: '同心培训', image: '/image/4.png', km: '298', address: '贵阳市观山湖区赤水大厦' },
      ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  pushSearch: function () {//跳转搜索页面
    wx.navigateTo({
      url: '/pages/search/search',
    })
  },
  coursesDetail: function (e) {//跳转课程详情
    var idx = e.currentTarget.dataset.idx
    wx.navigateTo({
      url: '/pages/courseDetails/courseDetails',
    })
  },
  pushSort: function (e) {//跳转分类页面
    var idx = e.currentTarget.dataset.idx
    wx.navigateTo({
      url: '/pages/sort/sort',
    })
  },
  pushSchool: function (e) {//跳转机构详情
    var idx = e.currentTarget.dataset.idx
    wx.navigateTo({
      url: '/pages/school/school',
    })
  },
  pushMoreCourses: function () {//跳转更多课程(课程搜索)
    wx.navigateTo({
      url: '/pages/moreCourse/moreCourse',
    })
  },
  pushMoreSchool: function () {//跳转更多学校(学校搜索)
    wx.navigateTo({
      url: '/pages/moreSchool/moreSchool',
    })
  }
})