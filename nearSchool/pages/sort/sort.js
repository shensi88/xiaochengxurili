// sort.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    menuList: ['距离', '区域', '类型'],
    distance: ['500m', '1000m', '1500m', '2000m'],
    area: ['南明区', '云岩区', '白云区', '金阳', '清镇'],
    typeList: ['语文', '数学', '英语', '物理', '化学'],
    showList: [],
    isShow:false,
    selectIdx:null,
    hotSchool: [
      { title: '方阵培训', image: '/image/fz.png', km: '298', address: '贵阳市贵开路居易阁10楼1号' },
      { title: '星火培训', image: '/image/2.png', km: '298', address: '贵阳市南明区龙洞堡' },
      { title: '同心培训', image: '/image/4.png', km: '298', address: '贵阳市观山湖区赤水大厦' },
    ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  showMenu: function (e) {//展示下拉菜单
    var idx = e.currentTarget.dataset.idx
    var that = this
    var showList = []
    if (idx == that.data.selectIdx){
      that.setData({
        isShow: false,
        selectIdx:null,
      })
    }else{
    if (idx == 0) {
      showList = that.data.distance
    }
    if (idx == 1) {
      showList = that.data.area
    }
    if (idx == 2) {
      showList = that.data.typeList
    }
    that.setData({
      showList: showList,
      selectIdx:idx,
      isShow:true,
  })
    }
  },
  cancelSelect:function(){//点击覆盖视图，消失视图
    var that = this
    that.setData({
      isShow: false,
      selectIdx: null,
    })
  },
  selectMenu:function(e){//选择菜单
    var idx = e.currentTarget.dataset.idx
    var that = this
    that.setData({
      isShow: false,
      selectIdx: null,
    })
  },
  pushSchool:function(e){//点击学校
    var idx = e.currentTarget.dataset.idx
    var that = this
    wx.navigateTo({
      url: '/pages/school/school',
    })
  }
})
