// search.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    input: false,//判断搜索框是在输入吗？如果是，则为true，搜索按钮显示 （取消）否则为false显示搜索
    focus: false,
    inputValue: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  obtainValue: function (e) {//获取输入
    this.data.inputValue = e.detail.value
  },
  changeSearchText: function () {
    this.setData({
      input: true,
      focus: true,
    })

  },
  cancelSearch: function () {//取消搜索
    this.setData({
      input: false,
      focus: false,
      inputValue: '',
    })
  },
  search: function () {//搜索
    console.log(this.data.inputValue)
  },
})