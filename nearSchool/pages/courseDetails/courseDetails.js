// courseDetails.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    topButton: ['课程详情', '相关课程', '学校介绍', '上课地图'],
    select: 0,
    markers: [{
      iconPath: "/image/address.png",
      id: 0,
      latitude: 26.596823,
      longitude: 106.718655,
      width: 50,
      height: 50
    }],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  makePhone: function () {//拨打电话
    wx.makePhoneCall({
      phoneNumber: '0851-86767827',
    })
  },
  changeSelect: function (e) {//切换选项
    console.log(e.currentTarget.dataset.idx)
    var that = this
    that.setData({
      select: e.currentTarget.dataset.idx
    })
  },
  pushMap: function () {//进入导航页
    wx.getLocation({
      type: 'gcj02', //返回可以用于wx.openLocation的经纬度  
      success: function (res) {
        var latitude = res.latitude
        var longitude = res.longitude
        wx.openLocation({
          latitude: latitude,
          longitude: longitude,
          name: "花园桥肯德基",
          scale: 28
        })
      }
    })  
  },

})