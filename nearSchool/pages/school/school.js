// pages/school/school.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    topButton: ['简介', '课程', '相册', '老师'],
    select: 0,
    infoList: [
      { image: '/image/cellPhone.png', title: '电话', content:'0851-86767827'},
      { image: '/image/schoolAdr.png', title: '地址', content: '贵州省贵阳市云岩区贵开路居易阁10楼1号' },
    ],
    imageList: ['/image/school.png', '/image/school.png']
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  changeSelect: function (e) {//切换选项
    console.log(e.currentTarget.dataset.idx)
    var that = this
    that.setData({
      select: e.currentTarget.dataset.idx
    })
  },
  courseDetails:function(e){//跳转课程详情
    var idx = e.currentTarget.dataset.idx
    var that = this
    wx.navigateTo({
      url: '/pages/courseDetails/courseDetails',
    })
  },
  tapDetail:function(e){//点击机构详情（拨打电话、地址导航）
  var idx = e.currentTarget.dataset.idx
  var that = this
  if(idx==0){
    wx.makePhoneCall({
      phoneNumber: '0851-86767827',
    })
  }else{//导航
    wx.getLocation({
      type: 'gcj02', //返回可以用于wx.openLocation的经纬度  
      success: function (res) {
        var latitude = res.latitude
        var longitude = res.longitude
        wx.openLocation({
          latitude: latitude,
          longitude: longitude,
          name: "花园桥肯德基",
          scale: 28
        })
      }
    })  
  }
  },
  showImage:function(e){//点击放大图片
    var idx = e.currentTarget.dataset.idx
    var that = this
    wx.previewImage({
      current: that.data.imageList[idx], // 当前显示图片的http链接
      urls: that.data.imageList // 需要预览的图片http链接列表
    })
  },
})