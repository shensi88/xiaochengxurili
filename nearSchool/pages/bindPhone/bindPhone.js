// bindPhone.js
var app = new getApp();
var util = require('../../utils/util.js');
function countdown(that) {
  var second = that.data.second
  if (second == 0) {
    that.setData({
      second: 60,
      buttonText: '发送验证码'
    });
    return;
  }
  var time = setTimeout(function () {
    that.setData({
      second: second - 1,
      buttonText: second - 1 + 's'
    });
    countdown(that);
  }
    , 1000)
}

Page({

  /**
   * 页面的初始数据
   */
  data: {
    phone: '',
    oldPhone: '',
    phoneAuth: false,
    buttonText: '发送验证码',
    second: 60,
    codeNum: '',
    smsId: '',//短信验证码id
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  codeNum: function (e) {//获取到 验证码输入框的内容
    var that = this
    that.data.codeNum = e.detail.value

  },
  phoneNum: function (e) {//获取到输入的手机号码
    var that = this
    var phoneNum = e.detail.value
    that.data.phone = phoneNum
  },
  sendCode:function(){//发送验证码
    var that = this
    if (that.data.phone.length == 0 || that.data.phone == null) {
      wx.showToast({
        title: '号码不可为空',
        duration: 1500
      })
      return;
    }
    countdown(that);
  },
  addData:function(){//提交数据 
    var that = this
    if (that.data.phone.length == 0 || that.data.phone==null){
       wx.showToast({
         title: '号码不可为空',
         duration:1500
       })
       return;
    }
    if (!util.validatemobile(that.data.phone)){
      return;
    }
    if (that.data.codeNum.length == 0 || that.data.codeNum == null) {
      wx.showToast({
        title: '请输入验证码',
        duration: 1500
      })
      return;
    }
  }
})