var app = new getApp();
// user.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    itmes: [{ imagepath: '/image/settledIn.png', title: '机构入驻', pageUrl:'/pages/settledIn/settledIn'},
      // { imagepath: '/image/bind.png', title: '绑定手机', pageUrl: '/pages/bindPhone/bindPhone'},
      { imagepath: '/image/service.png', title: '联系客服', pageUrl: '/pages/settledIn/settledIn'},
      { imagepath: '/image/aboutUs.png', title: '关于我们', pageUrl: '/pages/settledIn/settledIn'},
      { imagepath: '/image/feedback.png', title: '意见反馈', pageUrl: '/pages/feedback/feedback'}],
    heardImage:'',
    name:'',
    phoneAuth:'',
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this

    wx.getUserInfo({
      success: function (res) {
        console.log(res.userInfo)
        that.setData({
          heardImage: res.userInfo.avatarUrl,
          name: res.userInfo.nickName,
        })
      },
      fail: function (res) {
       
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  pushPage:function(e){
    var idx = e.currentTarget.dataset.idx
    var that = this
   
    wx.navigateTo({
      url: that.data.itmes[idx].pageUrl,
    })
  }
})