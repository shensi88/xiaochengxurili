function formatTime(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}


function validatemobile(mobile) {//判断手机号码是否正确
  if (mobile.length == 0) {
    wx.showToast({
      title: '请输入手机号！',
      icon: 'success',
      duration: 1500
    })
    return false;
  }
  if (mobile.length != 11) {
    wx.showToast({
      title: '手机号长度有误！',
      icon: 'success',
      duration: 1500
    })
    return false;
  }
  var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1}))+\d{8})$/;
  if (!myreg.test(mobile)) {
    wx.showToast({
      title: '手机号有误！',
      icon: 'success',
      duration: 1500
    })
    return false;
  }
  return true;
}
//获取时间年/月/日
function formatTimes(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()

  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/');
}
//获取时间年-月-日
function formatDateNow(date) {
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()
  var hour = date.getHours()
  var minute = date.getMinutes()
  var second = date.getSeconds()
  return [year, month, day].map(formatNumber).join('-');
}
//获取当前时间前一个月的时间
function formatBeforeMonth(date) {
  //获取系统前一个月的时间
  date.setMonth(date.getMonth() - 1);
  var y = date.getFullYear();
  var m = date.getMonth() + 1;
  var d = date.getDate();
  return [y, m, d].map(formatNumber).join('-');
}
function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}
//将时间戳转换为时间格式
function toDate(number) {
  var n = number;// 如果不是java后台需要乘以1000 var n = number * 1000;
  var date = new Date(n);
  var Y = date.getFullYear() + '/';
  var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '/';
  var D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
  return (Y + "-" + M + "-" + D + " "+ '(' + formatWeekday(number) + ')')
}
//将时间戳转换为时间年月日时分格式
function toMinDate(number) {
  var n = number ;
  var date = new Date(n);
  var Y = date.getFullYear() ;
  var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1);
  var D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
  var H = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
  var m = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
  return (Y +"-"+ M+"-" + D +" "+ H + ":" + m)
}

//将时间戳格式化为日期
function formatDateNum(timestamp) {
  var date = new Date(timestamp * 1000);
  return date.getMonth() + 1 + "月" + date.getDate() + "日 " + formatWeekday(timestamp);
}

//将时间戳格式化为时间
function formatTimeNum(timestamp) {
  var date = new Date(timestamp * 1000);
  return date.getHours() + ":" + date.getMinutes();

}
//将时间戳格式化为时间(小时)
function formatTimeNumHH(timestamp) {
  var date = new Date(timestamp);
  var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) ;
  var D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
  var H = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
  var m = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
  return date.getFullYear() + "-" + M + "-" + D + " " + H + ":" + m;
}
//中文形式的每周日期
function formatWeekday(timestamp) {

  var date = new Date(timestamp * 1000);
  var weekday = ["周日", "周一", "周二", "周三", "周四", "周五", "周六"];
  var index = date.getDay();
  return weekday[index];
}
//获取用户信息
function getUserInfo() {
  wx.getUserInfo({
    success: function (res) {
      var userInfo = res.userInfo
      var encryptedData = res.encryptedData
      var iv = res.iv
      return { 'userInfo': userInfo, 'encryptedData': encryptedData, 'iv': iv, 'success': true }
    },
    fail: function (res) {
      console.log('获取用户资料失败')
      return { 'success': false }
    }
  });

  return [year, month, day].map(formatNumber).join('-');
}

//缓存到本地
function setStorageKey(key, data) {
  wx.setStorageSync({
    key: key,
    data: data,
    success: function (res) {
      return true
    },
    fail: function (e) {
      return false
    }
  })
}
// 去掉字符串的空格
function Trim(str, is_global) {
  var result;
  result = str.replace(/(^\s+)|(\s+$)/g, "");
  if (is_global.toLowerCase() == "g") {
    result = result.replace(/\s/g, "");
  }
  return result;
}
// 去掉字符串前后的空格
function TrimStr(str) {
  return str.replace(/(^\s*)|(\s*$)/g, "");
}
module.exports = {
  formatTime: formatTime,
  validatemobile: validatemobile,
  formatTimes: formatTimes,
  toDate: toDate,
  formatDateNow: formatDateNow,
  formatDateNum: formatDateNum,
  formatTimeNum: formatTimeNum,
  formatBeforeMonth: formatBeforeMonth,
  getUserInfo: getUserInfo,
  setStorageKey: setStorageKey,
  Trim: Trim,
  TrimStr: TrimStr,
  formatTimeNumHH: formatTimeNumHH,
  toMinDate: toMinDate
}
